function deleteTheLine(o){
    var p = o.parentNode.parentNode;
    p.parentNode.removeChild(p);
}

// function deleteTheLine_v2(o){
//     var rowIndex = o.parentNode.parentNode.rowIndex;
//     document.getElementById('table_id').deleteRow(rowIndex);
// }


function changeBackground(o, color){
    var tdRef = getMyTDTag(o);
    // tdRef.parentNode.className="backgroundRed"
    tdRef.parentNode.style.backgroundColor=color;
}
function backtoInit(o){
    var tdRef = getMyTDTag(o);
    // tdRef.parentNode.className="backgroundInit";
    tdRef.parentNode.style.backgroundColor="white";
}



function getMyTDTag(ref){
    var theTDRef = ref;
    var found = false;
    while((theTDRef!==null)&&(!found)){
        if (theTDRef.nodeType===3){
            theTDRef = theTDRef.parentNode;
        } else if (theTDRef.tagName === "TD"){
            found = true;
        } else {
            theTDRef=theTDRef.parentNode;
        }
    }
    return theTDRef;
}

function refresh(){
    $.ajax({
        url:"/prweb/ajaxItemCategory.php",
        data: {
            code:"pandemonium"
        },
        method: "POST",
        success: function(result){
            removeLines();
            var obj = result.data;
            for(var i=0; i<obj.length;i++){
                addNewLine(obj[i].id, obj[i].title, obj[i].author, obj[i].body);
            }
            console.log("OK");
        },
        error: function(result, statut, erreur) {
            console.log("error")
        }
    });
}

function removeLines(){
    var listeTR = document.getElementsByTagName("TR");
    var toBeRemoved = new Array();
    for (var i=0;i<listeTR.length;i++){
        var ref = listeTR[i];
        if(ref.className==="line") {
            toBeRemoved.push(ref);
        }
    }
    while(toBeRemoved.length>0){
        ref = toBeRemoved.pop();
        ref.parentNode.removeChild(ref);
    }
}
function addNewLine(id, author, title, subject) {
    var string = "<tr class=\"line\">";
    string += "<td>"+id+"</td>";
    string += "<td>"+title+"</td>";
    string += "<td>"+author+"</td>";
    string += "<td>"+subject+"</td>";
    string += "<td style=\"border:none;\"><button class=\"btn\" onclick=\"deleteTheLine(this)\" onmouseover=\"changeBackground(this,'red')\" onmouseout=\"backtoInit(this)\"><img src=\"trash.png\"></button></td>";
    string += "</tr>";
    $('#myTable tr:last').after(string);
}
